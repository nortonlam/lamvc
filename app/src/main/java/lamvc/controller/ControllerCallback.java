package lamvc.controller;

import android.os.Bundle;

/**
 * User: norton
 * Date: 8/17/14
 */
public interface ControllerCallback {
  public void onControllerCompleted(Bundle params);
}
