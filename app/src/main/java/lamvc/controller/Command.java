package lamvc.controller;

import java.util.HashMap;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

/**
 * Created by IntelliJ IDEA.
 * User: Norton
 * Date: 5/7/11
 * Time: 12:10 AM
 */
public class Command {
  private static HashMap<Command, Controller> _commandMap;

  private String _name;

  protected Command(String name) {
    _name = name;
  }

  public static void init(Application app) {
  }

  public static void addCommand(Command command, Controller controller) {
    if (null == _commandMap) {
      _commandMap = new HashMap<Command, Controller>();
    }

    _commandMap.put(command, controller);
  }

  public static void execute(Command command, Activity context, Bundle params) {
    Controller controller = _commandMap.get(command);
    try {
      controller.execute(context, params);
    }
    catch (Exception e) {
      e.printStackTrace();

      controller.error(context, e);
    }
  }

  public static void executeLoading(Command command, Activity context, Bundle params) {
    executeLoading(command, context, params, null, null);
  }

  public static void executeLoading(Command command, Activity context, Bundle params, ControllerCallback controllerCallback) {
    Controller controller = _commandMap.get(command);

    LoadingController loading = new LoadingController(context, controller, params, controllerCallback, null);
    loading.execute();
  }

  public static void executeLoading(Command command, Activity context, Bundle params, DataCallback dataCallback) {
    Controller controller = _commandMap.get(command);

    LoadingController loading = new LoadingController(context, controller, params, null, dataCallback);
    loading.execute();
  }

  public static void executeLoading(Command command, Activity context, Bundle params, ControllerCallback controllerCallback, DataCallback dataCallback) {
    Controller controller = _commandMap.get(command);

    LoadingController loading = new LoadingController(context, controller, params, controllerCallback, dataCallback);
    loading.execute();
  }

  public void execute(Activity context, Bundle params) {
    execute(this, context, params);
  }

  public void executeLoading(Activity context, Bundle params) {
    executeLoading(this, context, params);
  }

  public void executeLoading(Activity context, Bundle params, ControllerCallback controllerCallback) {
    executeLoading(this, context, params, controllerCallback);
  }

  public void executeLoading(Activity context, Bundle params, DataCallback dataCallback) {
    executeLoading(this, context, params, dataCallback);
  }

  public void executeLoading(Activity context, Bundle params, ControllerCallback controllerCallback, DataCallback dataCallback) {
    executeLoading(this, context, params, controllerCallback, dataCallback);
  }

  public String toString() {
    return _name;
  }
}