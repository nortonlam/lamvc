package lamvc.controller;

import java.util.HashMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;

/**
 * Created by IntelliJ IDEA.
 * User: Norton
 * Date: 5/9/11
 * Time: 8:29 PM
 */
public class LoadingController extends AsyncTask<Void, Void, Boolean> {
  private Activity _context;
  private ProgressDialog _progressDialog;
  private Controller _controller;
  private Bundle _params;
  private ControllerCallback _controllerCallback;
  private DataCallback _dataCallback;

  private Exception _exception;

  public LoadingController(Activity context, Controller controller, Bundle params, ControllerCallback controllerCallbadk, DataCallback dataCallback) {
    _context = context;
    _controller = controller;
    _params = params;
    _controllerCallback = controllerCallbadk;
    _dataCallback = dataCallback;
  }

  @Override
  public void onPreExecute() {
    if (!TextUtils.isEmpty(_controller.getProgressMessage())) {
      _progressDialog = ProgressDialog.show(_context, "", _controller.getProgressMessage());
    }
  }

  @Override
  protected Boolean doInBackground(Void... params) {
    try {
      _controller.execute(_context, _params);
    }
    catch (Exception e) {
      e.printStackTrace();

      _exception = e;
      return false;
    }

    return true;
  }

  @Override
  protected void onPostExecute(Boolean result)
  {
    if (_progressDialog != null)
    {
      _progressDialog.hide();
    }

    if (false == result)
    {
      _controller.error(_context, _exception);
    }

    if (null != _controllerCallback)
    {
      _controllerCallback.onControllerCompleted(_controller.getCallbackParams());
    }

    if (null != _dataCallback)
    {
      _dataCallback.onDataLoaded(_controller.getModel());
    }
  }
}
