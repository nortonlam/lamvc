package lamvc.controller;

import lamvc.model.Model;

/**
 * User: norton
 * Date: 8/11/14
 */
public interface DataCallback {
  public void onDataLoaded(Model model);
}
