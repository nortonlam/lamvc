package lamvc.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import lamvc.ui.Params;

/**
 * Created by IntelliJ IDEA.
 * User: Norton
 * Date: 5/24/11
 * Time: 6:28 PM
 */
public class ActivityController {
  private String _action;

  protected ActivityController(String action) {
    _action = action;
  }

  public static void start(Context context, ActivityController action) {
    start(context, action, 0);
  }

  public static void start(Context context, ActivityController action, int flags) {
    Intent activityIntent = new Intent();
    if (0 != flags) {
      activityIntent.setFlags(flags);
    }
    activityIntent.setAction(action.getAction());

    context.startActivity(activityIntent);
  }

  public static void start(Context context, ActivityController action, String errorMessage) {
    start(context, action, 0, errorMessage);
  }

  public static void start(Context context, ActivityController action, int flags, String errorMessage) {
    Intent activityIntent = new Intent();
    if (0 != flags) {
      activityIntent.setFlags(flags);
    }
    activityIntent.setAction(action.getAction());
    activityIntent.putExtra(Params.ERROR_MESSAGE, errorMessage);

    context.startActivity(activityIntent);
  }

  public static void start(Context context, ActivityController action, Bundle params) {
    start(context, action, params, 0);
  }

  public static void start(Context context, ActivityController action, Bundle params, int flags) {
    Intent activityIntent = new Intent();
    if (0 != flags) {
      activityIntent.setFlags(flags);
    }
    activityIntent.setAction(action.getAction());
    activityIntent.putExtras(params);

    context.startActivity(activityIntent);
  }

  public static void startForResult(Activity context, ActivityController action, int requestCode) {
    startForResult(context, action, 0, requestCode);
  }

  public static void startForResult(Activity context, ActivityController action, int flags, int requestCode) {
    Intent activityIntent = new Intent();
    if (0 != flags) {
      activityIntent.setFlags(flags);
    }
    activityIntent.setAction(action.getAction());

    context.startActivityForResult(activityIntent, requestCode);
  }

  public String getAction() {
    return _action;
  }

  public void start(Context context) {
    start(context, this);
  }

  public void start(Context context, int flags) {
    start(context, this, flags);
  }

  public void start(Context context, String errorMessage) {
    start(context, this, errorMessage);
  }

  public void start(Context context, int flags, String errorMessage) {
    start(context, this, flags, errorMessage);
  }

  public void start(Context context, Bundle params) {
    start(context, this, params);
  }

  public void start(Context context, Bundle params, int flags) {
    start(context, this, params, flags);
  }

  public void startForResult(Activity context, int requestCode) {
    startForResult(context, this, requestCode);
  }

  public void startForResult(Activity context, int flags, int requestCode) {
    startForResult(context, this, flags, requestCode);
  }
}
