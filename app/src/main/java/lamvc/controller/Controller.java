package lamvc.controller;

import java.util.HashMap;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import lamvc.model.Model;

/**
 * Created by IntelliJ IDEA.
 * User: Norton
 * Date: 5/7/11
 * Time: 12:12 AM
 */
public abstract class Controller {
  protected Application _app;
  private String _progressMessage;

  private Bundle _callbackParams;
  private Model _model;

  protected Controller(Application app) {
    _app = app;
  }

  protected Controller(Application app, String progressMessage) {
    _app = app;
    _progressMessage = progressMessage;
  }

  protected Application getApplication() {
    return _app;
  }

  public String getProgressMessage() {
    return _progressMessage;
  }

  public abstract void execute(Activity context, Bundle params) throws Exception;

  public Bundle getCallbackParams() {
    return _callbackParams;
  }

  protected void setCallbackParams(Bundle params) {
    _callbackParams = params;
  }

  public Model getModel() {
    return _model;
  }

  protected void setModel(Model model) {
    _model = model;
  }

  public abstract void error(Activity context, Exception exception);
}