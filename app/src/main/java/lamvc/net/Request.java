package lamvc.net;

import java.io.IOException;
import java.util.HashMap;

import org.xmlpull.v1.XmlSerializer;

/**
 * Created by IntelliJ IDEA.
 * User: Norton
 * Date: 4/21/11
 * Time: 12:37 AM
 */
public abstract class Request {
  private String _url;

  protected int _mode = ServerMode.BUILD;

  protected Request() {
  }

  protected Request(String url) {
    _url = url;
  }

  public String getUrl() {
    return _url;
  }

  public HashMap<String, String> getHeaders() {
    return null;
  }

  public abstract String build() throws IOException;
  public abstract String getOfflineResponseStr();
  protected abstract void addDataXml(XmlSerializer serializer) throws IOException;

  public int getMode() {
    return _mode;
  }

  public abstract Response getResponseInstance(String responseStr);
}