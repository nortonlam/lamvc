package lamvc.net;

/**
 * Created by IntelliJ IDEA.
 * User: Norton
 * Date: 4/30/11
 * Time: 10:01 PM
 */
public abstract class Response {
  protected final static String SUCCESS = "success";
  protected final static String ERROR = "error";

  private String _responseStr;

  private String _status;
  private String _responseCode;
  private String _systemMessage;
  private String _userMessage;

  public Response(String responseStr) {
    _responseStr = responseStr;

    try {
      parseResponse(responseStr);
    }
    catch (ParseException e) {
      _status = ERROR;

      e.printStackTrace();
    }
  }

  public abstract void parseResponse(String responseStr) throws ParseException;

  public void setStatus(String status) {
    _status = status;
  }

  public boolean success() {
    return SUCCESS.equals(_status);
  }

  public String getResponseCode() {
    return _responseCode;
  }

  public void setResponseCode(String responseCode) {
    _responseCode = responseCode;
  }

  public String getResponseStr() {
    return _responseStr;
  }

  public String getSystemMessage() {
    return _systemMessage;
  }

  public void setSystemMessage(String systemMessage) {
    _systemMessage = systemMessage;
  }

  public String getUserMessage() {
    return _userMessage;
  }

  public void setUserMessage(String userMessage) {
    _userMessage = userMessage;
  }
}