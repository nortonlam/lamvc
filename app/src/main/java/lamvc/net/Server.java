package lamvc.net;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xmlpull.v1.XmlSerializer;

import java.io.*;
import java.net.*;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Norton
 * Date: 4/21/11
 * Time: 12:13 AM
 */
public class Server {
  public static final int THIRTY_SECOND_TIMEOUT = 30 * 1000;
  public static final int ONE_MINUTE_TIMEOUT = 60 * 1000;
  public static final int TWO_MINUTE_TIMEOUT = 2 * 60 * 1000;
  public static final int FIVE_MINUTE_TIMEOUT = 5 * 60 * 1000;

  private final String TAG = getClass().getCanonicalName();

  private String _urlStr;

  private boolean _isOffline = false;

  public Server(String url) throws MalformedURLException {
    _urlStr = url;
    if (!_urlStr.startsWith("http")) {
      _urlStr = "http://" + _urlStr;
    }
    Log.i(TAG, "url: " + url);

    Log.i(TAG, "full url: " + _urlStr);
  }

  public static CookieStore getCookieStore() {
    return ((CookieManager) CookieHandler.getDefault()).getCookieStore();
  }

  public void setIsOffline(boolean flag) {
    _isOffline = flag;
  }

  public Response sendGetRequest(Request request) throws IOException {
    return sendGetRequest(request, ONE_MINUTE_TIMEOUT);
  }

  public Response sendGetRequest(Request request, int timeout) throws IOException {
    if (_isOffline) {
      return request.getResponseInstance(request.getOfflineResponseStr());
    }

    CookieStore cookies = Server.getCookieStore();
    Log.d(getClass().getCanonicalName(), "cookie store size: " + cookies.getCookies().size());

    for (HttpCookie cookie : cookies.getCookies()) {
      Log.d(getClass().getCanonicalName(), "cookie: " + cookie.toString());
    }

    String requestURL;
    if (ServerMode.PREBUILT == request.getMode()) {
      requestURL = request.build();
    } else {
      requestURL = new StringBuilder(_urlStr).append(request.build()).toString();
    }
    Log.i(getClass().getName(), "requestURL: " + requestURL);

    URL url = new URL(requestURL);
    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
    connection.setRequestMethod("GET");
    connection.setDoInput(true);
    connection.setUseCaches(false);
    connection.setReadTimeout(timeout);

    if (request.getHeaders() != null) {
      Iterator it = request.getHeaders().entrySet().iterator();
      while (it.hasNext()) {
        Map.Entry header = (Map.Entry) it.next();
        connection.setRequestProperty(header.getKey().toString(), header.getValue().toString());
      }
    }

    int serverResponseCode = connection.getResponseCode();
    Log.i(TAG, "Status code: " + serverResponseCode);
    String serverResponseMessage = connection.getResponseMessage();
    Log.i(TAG, "Server message: " + serverResponseMessage);

    if (200 == serverResponseCode) {
      LineNumberReader lnr = new LineNumberReader(new InputStreamReader(connection.getInputStream()));
      String line = "";
      String responseTxt = "";
      Log.i(TAG, "Response received:\n" + responseTxt);
      while (null != (line = lnr.readLine())) {
        responseTxt += line.trim() + "\n";
        Log.i("", line.trim() + "\n");
      }


      return request.getResponseInstance(responseTxt);
    } else if (302 == serverResponseCode) {
      String redirectUrl = connection.getHeaderField("Location");
      android.util.Log.i(getClass().getName(), "redirectUrl: " + redirectUrl);

      RedirectRequest redirectRequest = new RedirectRequest(redirectUrl, request);
      Response response = sendGetRequest(redirectRequest);

      return response;
    }

    throw new IOException("Error communicating with server: " + serverResponseCode + " : " + serverResponseMessage);
  }

  public Response sendPostRequest(Request request) throws IOException {
    return sendPostRequest(request, ONE_MINUTE_TIMEOUT);
  }

  public Response sendPostRequest(Request request, int timeout) throws IOException {
    if (_isOffline) {
      return request.getResponseInstance(request.getOfflineResponseStr());
    }

    CookieStore cookies = Server.getCookieStore();
    Log.d(getClass().getCanonicalName(), "cookie store size: " + cookies.getCookies().size());

    for (HttpCookie cookie : cookies.getCookies()) {
      Log.d(getClass().getCanonicalName(), "cookie: " + cookie.toString());
    }

    URL url = new URL(_urlStr);
    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
    connection.setRequestMethod("POST");
    connection.setDoInput(true);
    connection.setDoOutput(true);
    connection.setUseCaches(false);
    connection.setReadTimeout(timeout);

    if (request.getHeaders() != null) {
      Iterator it = request.getHeaders().entrySet().iterator();
      while (it.hasNext()) {
        Map.Entry header = (Map.Entry) it.next();
        connection.setRequestProperty(header.getKey().toString(), header.getValue().toString());
      }
    }

    String requestData = request.build();
    Log.i(getClass().getName(), "Request data: " + requestData);
    PrintWriter pw = new PrintWriter(connection.getOutputStream());

    pw.println(requestData);
    pw.close();
    Log.i(getClass().getName(), "Request sent.");

    // Print out the response headers
    /*java.util.Map headerFields = connection.getHeaderFields();
    java.util.Set headers = headerFields.entrySet();
    for(Iterator i = headers.iterator(); i.hasNext();){
      Map.Entry map = (Map.Entry)i.next();
      Log.v("Server.url", "response header key:"+map.getKey()+"  with value of:"+map.getValue());
    }*/

    int serverResponseCode = connection.getResponseCode();
    Log.i(getClass().getName(), "Status code: " + serverResponseCode);
    String serverResponseMessage = connection.getResponseMessage();
    Log.i(getClass().getName(), "Server message: " + serverResponseMessage);

    if (200 == serverResponseCode) {
      for (Map.Entry<String, List<String>> header : connection.getHeaderFields().entrySet()) {
        System.out.println(header.getKey() + "=" + header.getValue());
      }

      LineNumberReader lnr = new LineNumberReader(new InputStreamReader(connection.getInputStream()));
      String line = "";
      String responseTxt = "";
      while (null != (line = lnr.readLine())) {
        responseTxt += line + "\n";
      }

      Log.i(TAG, "Response received:\n" + responseTxt);

      return request.getResponseInstance(responseTxt);
    } else if (302 == serverResponseCode) {
      String redirectUrl = connection.getHeaderField("Location");
      android.util.Log.i(getClass().getName(), "redirectUrl: " + redirectUrl);

      RedirectRequest redirectRequest = new RedirectRequest(redirectUrl, request);
      Response response = sendGetRequest(redirectRequest);

      return response;
    }

    throw new IOException("Error communicating with server: " + serverResponseCode + " : " + serverResponseMessage);
  }

  private class RedirectRequest extends Request {
    private Request _originalRequest;

    private RedirectRequest(String url, Request originalRequest) {
      super(url);

      _mode = ServerMode.PREBUILT;
      _originalRequest = originalRequest;
    }

    @Override
    public String build() throws IOException {
      return getUrl();
    }

    @Override
    public String getOfflineResponseStr() {
      return null;
    }

    @Override
    protected void addDataXml(XmlSerializer serializer) throws IOException {

    }

    @Override
    public Response getResponseInstance(String responseStr) {
      return _originalRequest.getResponseInstance(responseStr);
    }
  }
}