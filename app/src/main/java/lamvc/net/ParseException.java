package lamvc.net;

/**
 * Author: norton
 * <p/>
 * Created on: 9/2/14.
 */
public class ParseException extends Exception {
  public ParseException(String message) {
    super(message);
  }
}
